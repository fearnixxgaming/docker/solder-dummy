FROM registry.gitlab.com/fearnixxgaming/docker/alpine:latest

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]